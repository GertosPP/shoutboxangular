(function () {
    'use strict'
    angular.module('app')
        .controller('ShoutboxController', ['$http','$location',ShoutboxController])

    function ShoutboxController($http,$location) {
        var vm = this
        vm.header = "Shoutbox for AiS"
        vm.getDataFromApi = function getDataFromApi(timeout) {
            var params = $location.search();
            if (!params.take)
                params.take = 10;
            if (!params.skip)
                params.skip = 0;
            $http.get("/api/ShoutMessages", {params:params, timeout: timeout })
                .then(function (response) {
                    vm.messages = response.data
                },function (e){
                    vm.error = e
                });
        };
        vm.getDataFromApi(1000)
        setInterval(vm.getDataFromApi, 1000, {timeout:1000})
    }
})()